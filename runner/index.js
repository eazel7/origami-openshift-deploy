var fs = require('fs'), async = require('async');

var linker = require('./linker');
async.each(Object.keys(linker), function (mod, callback) {
  var dst = './node_modules/' + mod, src = '../' + linker[mod].path;
  
  if (!fs.existsSync(dst)) fs.symlink(src, dst, callback);
  else callback();
}, function (err) {
  if (err) {
    console.log(err);
    return process.exit(-1);
  }
  
  require('./manager');
});
