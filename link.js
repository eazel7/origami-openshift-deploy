var moduleNames = ['api-mongo', 'app', 'auth-local', 'manager', 'random-names'];

var allDeps = {}, noMismatch = true, interDeps = {}, origNames = {};

for (var i = 0; i < moduleNames.length; i++) {
  var jsonPath = './preparing/base/' + moduleNames[i] + '/package.json';
  var json = require(jsonPath);

  origNames[json.name] = moduleNames[i];
  interDeps[moduleNames[i]] = [];
}

for (var i = 0; i < moduleNames.length; i++) {
  var jsonPath = './preparing/base/' + moduleNames[i] + '/package.json';
  var json = require(jsonPath);
  
  if (json.dependencies) {
    for (var d in json.dependencies) {
      if (origNames[d]) {
        interDeps[moduleNames[i]].push(d);
      } else if (!allDeps[d]) {
        allDeps[d] = {
          version: json.dependencies[d],
          in: [moduleNames[i]]
        }
      } else {
        if (allDeps[d].version !== json.dependencies[d]) {
          console.log('dependency mismatch: ' + d + ' ' + allDeps[d].version);
          console.log('other modules using it: ' + allDeps[d].in.join('j'));
          console.log('mismatching with: ' + moduleNames[i]);
          console.log('which uses: ' + json.dependencies[d]);
          noMismatch = false;
        } else {
          allDeps[d].in.push(moduleNames[i]);
        }
      }
    }
  }
}

if (noMismatch) {
  var finalJson = require('./preparing/runner/package.json');
  
  for (var d in allDeps) {
    finalJson.dependencies[d] = allDeps[d].version;
  }
  
  var fs = require('fs'), async = require('async');
  fs.writeFile("preparing/runner/package.json", JSON.stringify(finalJson, undefined, 2), function(err) {
      if(err) {
          console.error(err);
          return process.exit(-1);
      }
      
      async.each(moduleNames, function (m, callback) {
        fs.rename('preparing/base/' + m, 'preparing/runner/' + m, callback);
      }, function (err) {
        if (err) {
          console.error(err);
          return process.exit(-1);
        }
        
        var linker = {};
        
        async.each(moduleNames, function (m, callback) {
          var orig = require('./preparing/runner/' + m + '/package.json');
          
          linker[orig.name] = {
            path: './' + m,
            interDeps: interDeps[m]
          };
          
          callback();
        }, function (err) {
          if (err) {
            console.error(err);
            return process.exit(-1);
          }
        
          fs.writeFile("preparing/runner/linker.json", JSON.stringify(linker, undefined, 2), function(err) {
            if (err) {
              console.error(err);
              return process.exit(-1);
            }
            
            console.log('runner ready');
            process.exit(0);
          });
        });
      });
  }); 
}
