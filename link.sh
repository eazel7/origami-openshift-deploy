#! /bin/bash

if [ -d "./base" ]; then
  (cd base && git checkout . && git clean -f && git pull)
else
  git clone git@gitlab.com:eazel7/origami-main base
fi

if [ -d "./openshift-repo" ]; then
  (cd openshift-repo && git checkout . && git clean -f && git pull)
else
  echo "Please enter the OpenShift application name (just a-Z and 0-9)"
  read APPNAME
  rhc app-create "$APPNAME" nodejs-0.10 mongodb-2.4 -r openshift-repo
fi

rm -fr ./openshift-repo/*
rm -fr preparing/base
rm -fr preparing/runner

rsync -rv --exclude=.git base preparing
rsync -rv --exclude=.git --exclude=node_modules runner preparing

node link.js

(cd openshift-repo && rm -fr ./*)
rsync -rv --exclude=.git preparing/runner/* openshift-repo
(cd openshift-repo && git add . && git commit -a -m "new deploy" && git push)
